---
title: "PIKNIK NAUKOWY 2006 - Miejsce konferencji"
permalink: /cieszyn/
---

<span class="header">Miejsce konferencji - o Cieszynie</span>

<img src="{{ "../images/herb_cieszyn.png" | absolute_url }}" alt="Herb Cieszyna" class="imgRight" />

Malowniczo położony u podnóża Beskidu Śląskiego, nad rzeką Olzą, Cieszyn jest historyczną stolicą regionu. Podanie
głosi, że w 810 roku trzej bracia: Cieszko, Bolko i Mieszko, spotkali się przy tutejszym źródle po długiej wędrówce i
ciesząc się tym faktem założyli miasto Cieszyn. Do dzisiaj Studnia Trzech Braci ma upamiętniać to wydarzenie. Badania
naukowe stwierdzają jednak, że historia cieszyńskiego grodu sięga znacznie dalej, niż głosi podanie.

Obecnie miasto podzielone jest wzdłuż Olzy na części polską i czeską, przy czym historyczna część miasta pozostała
po stronie polskiej. Przez długie lata Cieszyn był jednym z najważniejszych grodów w tej części Europy, jednak w XX w.
utracił swą dominującą pozycję w regionie na rzecz przemysłowych Bielska i Ostrawy, co utrwalił jeszcze podział miasta
w 1920 roku, który sprowadził Cieszyn do roli miasta granicznego.

Wichry XX-wiecznych wojen oszczędziły ten stary piastowski gród, co przy niewielkim stopniu uprzemysłowienia i rozbudowy
pozwoliło zachować Cieszynowi jego specyficzny charakter i klimat. Składa się na nie zarówno piękno starego miasta,
z niezliczonymi zabytkami, jak i stosunkowo duża ilość rdzennej ludności, często rozpamiętującej dawną świetność
i niezależność swego miasta i regionu.

Nazywany "małym Wiedniem" Cieszyn jest podstawowym punktem programu każdego szanującego się turysty przebywającego
w okolicy. Mimo niewielkich rozmiarów miasto pozostało ważnym ośrodkiem kulturalnym, oświatowym i administracyjnym,
a część zabytkowa miasta może ująć naprawdę każdego...

[Żródło](http://www.ksiestwocieszynskie.republika.pl/cieszyn.html)

<span class="header">Zabytki</span>

<img src="{{ "../images/cieszyn_rotunda.jpg" | absolute_url }}" alt="Cieszyńska Rotunda" class="imgLeft" />

Okolice, jak i sam Cieszyn, posiadają wiele zabytków. W tym roku logiem konferencji jest jeden z nich -
Rotunda romańska z XI w. w kościele św. Mikołaja i św. Wacława.

[Więcej o zabytkach](http://www.ksiestwocieszynskie.republika.pl/zabcieszyn.html)

<span class="header">Dojazd</span>
Do Cieszyna najłatwiej dostać się pociągiem. Istnieje kilka dogodnych połączeń:

* Katowice - Cieszyn (przez Zebrzydowice)
* Kraków - Cieszyn (przez Czechowice-Dziedzice)
* Warszawa - Cieszyn (przez Czechowice-Dziedzice)
* Poznań/Wrocław - Cieszyn (przez Katowice)
* Zielona Góra - Cieszyn (przez Katowice)

Więcej informacji na stronie [PKP](http://rozklad.pkp.pl/).

<span class="header">Plan miasta</span>

{: style="text-align:center"}
<img src="{{ "../images/map.png" | absolute_url }}" /><br />
Na planie zaznaczono miejsca zakwaterowania uczestników konferencji oraz dworzec PKP.<br />
Plan opracowany na podstawie internetowego planu Cieszyna z [http://www.cieszyn.pl/](http://www.cieszyn.pl/).

<span class="header">Linki</span>

[Strona WWW Cieszyna](http://www.cieszyn.pl/)
