---
title: "PIKNIK NAUKOWY 2006 - Informacje naukowe"
permalink: /konferencja/
---

<span class="header">Informacje naukowe</span>

## Proponowane sesje wykładowe

* **Fizyka doświadczalna i stosowana**
  - Dydaktyka Fizyki
  - Zastosowania Fizyki (m.in. fizyka medyczna)
  - Fizyka Fazy Skondensowanej
  - Fizyka Jądrowa i Jej Zastosowania
* **Fizyka teoretyczna**
  - Teoria Ciała Stałego
  - Metody Komputerowe Fizyki
  - Teoria Pola i Cząstki Elementarne
  - Astrofizyka i Kosmologia
  - Fizyka Współczesna


Przygotowywane wykłady powinny mieć charakter przystępny dla ogółu uczestników konferencji. Przewidywany czas na jedno
wystąpienie to ok. 25 minut, w co wliczone jest od 5 do 10 minut pytań i dyskusji. Przedłużające się dyskusje będziemy
przenosić do "kuluarów".

<span class="header">Uczestnicy konferencji</span>

{% assign participants = site.data.participants | sort: "name" | sort: "surname" %}

<ol>
  {% for participant in participants %}
  {% if participant.degree == "dr" or participant.degree == "dr hab." or participant.degree == "doc." or participant.degree == "prof." %}
  <li>{{ participant.degree | append: ' ' }}{{ participant.name | append: ' ' | append: participant.surname }} ({{ participant.university }}){% if participant.desc != "" %}{{ " - " | append: participant.desc }}{% endif %}</li>
  {% endif %}
  {% endfor %}
</ol>

## Studenci

<ol>
  {% for participant in participants %}
  {% if participant.degree == "" or participant.degree == "mgr" or participant.degree == "inż." or participant.degree == "mgr inż." %}
  <li>{% if participant.degree %}{{ participant.degree | append: ' ' }}{% endif %}{{ participant.name | append: ' ' | append: participant.surname }} ({{ participant.university }})</li>
  {% endif %}
  {% endfor %}
</ol>

<span class="header">Plan konferencji</span>

<table class="schedule">
<tr><th style="width: 15%;">godz.</th><th>czwartek (20.04)</th><th>piątek (21.04)</th><th>sobota (22.04)</th><th>niedziela (23.04)</th></tr>
<tr><td>08:00 - 08:30</td>
	<td rowspan="11">rejestracja uczestników</td><td rowspan="2">&nbsp;</td><td rowspan="2">&nbsp;</td><td rowspan="2">&nbsp;</td>
</tr>
<tr><td>08:30 - 09:00</td></tr>
<tr><td>09:00 - 09:30</td>	<td rowspan="3">śniadanie</td><td rowspan="3">śniadanie</td><td rowspan="3">śniadanie</td></tr>
<tr><td>09:30 - 10:00</td></tr>
<tr><td>10:00 - 10:30</td></tr>
<tr><td>10:30 - 11:00</td>
	<td rowspan="6">I sesja wykładowa</td><td rowspan="6">III sesja wykładowa</td><td rowspan="4">V sesja wykładowa</td>
</tr>
<tr><td>11:00 - 11:30</td></tr>
<tr><td>11:30 - 12:00</td></tr>
<tr><td>12:00 - 12:30</td></tr>
<tr><td>12:30 - 13:00</td>		
	<td rowspan="4">uroczyste zakończenie konferencji oraz obiad</td>
</tr>
<tr><td>13:00 - 13:30</td></tr>
<tr><td>13:30 - 14:00</td><td rowspan="3">obiad</td><td rowspan="3">obiad</td><td rowspan="3">obiad</td></tr>
<tr><td>14:00 - 14:30</td></tr>
<tr><td>14:30 - 15:00</td>
	<td rowspan="14">&nbsp;</td>
</tr>
<tr><td>15:00 - 15:30</td><td rowspan="3">czas wolny</td><td rowspan="6">II sesja wykładowa</td><td rowspan="6">IV sesja wykładowa</td></tr>
<tr><td>15:30 - 16:00</td></tr>
<tr><td>16:00 - 16:30</td></tr>
<tr><td>16:30 - 17:00</td><td rowspan="3">wykłady inauguracyjne</td></tr>
<tr><td>17:00 - 17:30</td></tr>
<tr><td>17:30 - 18:00</td></tr>
<tr><td>18:00 - 18:30</td>
<td rowspan="4">powitalna lampka wina - uroczysta kolacja - rozpoczęcie konferencji (sugerowane odpowiednie stroje :))</td>
<td rowspan="2">kolacja</td>
<td rowspan="2">kolacja</td></tr>
<tr><td>18:30 - 19:00</td></tr>
<tr><td>19:00 - 19:30</td>
	<td rowspan="5">impreza integracyjna w klubie studenckim do bladego świtu ;)</td>
	<td rowspan="5">proponowana wycieczka do Czeskiego Cieszyna</td>
</tr>
<tr><td>19:30 - 20:00</td></tr>
<tr><td>20:00 - 20:30</td><td rowspan="3">&nbsp;</td></tr>
<tr><td>20:30 - 21:00</td></tr>
<tr><td class="nowrap">21:00 - +\infty ;)</td></tr>
</table>

W razie konieczności zostanie **zmniejszona** liczba sesji wykładowych. W tracie konferencji przewidujemy sesję posterową.
