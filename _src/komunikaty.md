---
title: "PIKNIK NAUKOWY 2006 - Konferencja"
permalink: /komunikaty/
---

<span class="header">Komunikaty</span>

* **UWAGA:** Ostateczny termin rejestracji i uiszczenia całości opłaty konferencyjnej mija 14. kwietnia!
* Prosimy o wpłaty **całości opłaty konferencyjnej** na konto: ING BANK ŚLĄSKI,
  <span class="nowrap">74 1050 1214 1000 0007 0000 7909</span>, z dopiskiem: **"KNF-PIKNIK2006"**.
* Jeśli przelewu na konto dokona uczelnia, Uniwersytet Śląski automatycznie wystawi fakturę, która zostanie wysłana
	do wpłacającego listem poleconym. Osoby fizyczne dokonujące wpłat i chcące otrzymać fakturę proszone są o kontakt
	mailowy na adres [agrzanka@us.edu.pl](mailto:agrzanka@us.edu.pl). W miarę możliwości, faktury dla osób fizycznych
	zostaną wystawione podczas konferencji.
* Jedno koło naukowe może na konferencji reprezentować maksymalnie 7 osób, z czego przynajmniej jedna powinna wygłosić wykład.
* Przypominamy o potrzebie podania NIP-u oraz adresu uczelni w celu wystawienia faktury VAT oraz okazania podczas
	konferencji potwierdzeń wpłaty opłat konferencyjnych oraz zaliczek.

<span class="header">Zakwaterowanie i opłaty</span>

## Miejsce konferencji i zakwaterowanie

* Wykłady, dyskusje:<br />
	**Centrum Konferencyjne w Cieszynie**<br />
	43 - 400 Cieszyn, ul. Bielska 62<br />
	tel. 033 - 854 61 52<br />
	fax. 033 - 854 63 21<br /><br />
* Zakwaterowanie (kadra):<br />
	**Zajazd Academicus**<br />
	43 - 400 Cieszyn, ul. Paderewskiego 6<br />
	tel. 033 - 8546464<br />
	fax: 033 - 8546466<br /><br />
* Zakwaterowanie (studenci):<br />
	**DSN -- akademik**<br />
	43-400 Cieszyn, ul. Niemcewicza 8<br />
	tel. 0338546130 - kierowniczka osiedla<br />
	tel./fax. 0338546127 - administracja<br />
	tel. 0338546120 - recepcja<br />

## Opłaty

* studenci: 150 PLN
* pracownicy naukowi: 270 PLN

W opłatę konferencyjną wliczony jest koszt noclegu w akademiku oraz wyżywienia.

<span class="header">Plan miasta</span>

{: style="text-align:center"}
<img src="{{ "../images/map.png" | absolute_url }}" /><br />
Na planie zaznaczono miejsca zakwaterowania uczestników konferencji oraz
dworzec PKP. <br />
Plan opracowany na podstawie internetowego planu Cieszyna z [http://www.cieszyn.pl/](http://www.cieszyn.pl/).


<span class="header">Pliki</span>

* [Plakat konferencji (100 dpi)]({{ "../files/okknf-2006-plakat.png" | absolute_url }})
* [Animacja reklamowa]({{ "../files/Piknik_Naukowy_2006.swf" | absolute_url }})
* [Krótki film reklamowy]({{ "../files/konf2006.avi" | absolute_url }})
* [Książka konferencja]({{ "../files/okknf-2006-abstract-book.pdf" | absolute_url }})
