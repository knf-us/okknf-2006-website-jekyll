---
title: "PIKNIK NAUKOWY 2006 - Informacje"
permalink: /info/
---

<span class="header">Komitet organizacyjny</span>

Organizatorami Konferencji są: [Koło Naukowe Fizyków Uniwersytetu Śląskiego w Katowicach](http://knf.us.edu.pl/)
oraz Sekcja Młodych Polskiego Towarzystwa Fizycznego. Komitet organizacyjny:

## Główny Komitet Organizacyjny

* Andrzej Ptok - prezes KNF UŚ (2005/2006), wiceprzewodniczący SM PTF
* Agnieszka Grzanka - viceprezes KNF UŚ ds. finansowych
* Artur Fijałkowski

## Wykonawczy Komitet Organizacyjny

* Katarzyna Bartuś - viceprezes KNF UŚ ds. naukowych
* Michał Januszewski - prezes KNF UŚ (2006/2007)

Pytania i ew. zgłoszenia udziału prosimy kierować na adres e-mailowy: [agrzanka@us.edu.pl](mailto:agrzanka@us.edu.pl).

<span class="header">Sponsorzy</span>

* [Uniwersytet Śląski w Katowicach](http://www.us.edu.pl/)
* [Polskie Towarzystwo Fizyczne](http://ptf.fuw.edu.pl/)
