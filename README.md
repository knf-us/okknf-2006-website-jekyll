# okknf-2006-website-jekyll
OKKNF 2006 website converted to Jekyll

## Compilation

To complile [Jekyll](https://jekyllrb.com/) project to HTML website run

> jekyll build

To serve website on local development server run

> jekyll serve

You need Jekyll 3.6.0 to compile the website - `link` filter doesn't work with Jekyll 3.1.6.
